import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { SlotPageComponent } from './slot-page/slot-page.component';

const routes: Routes = [
  {path:'',component:HomePageComponent},
  {path:'slot_game',component:SlotPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
