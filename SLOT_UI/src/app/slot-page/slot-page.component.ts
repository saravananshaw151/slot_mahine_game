import { Component, OnInit } from '@angular/core';
import { SlotImageService } from '../services/slot-image.service';
import { Router } from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import { PaylinesComponent } from '../paylines/paylines.component';

interface ImageInfo{
  imageLink:string |undefined,
  highlight:boolean | undefined
}

@Component({
  selector: 'app-slot-page',
  templateUrl: './slot-page.component.html',
  styleUrls: ['./slot-page.component.scss']
})
export class SlotPageComponent implements OnInit {

  reels:any[][] = [];
  reel1:any[]=[];
  reel2:any[]=[];
  reel3:any[]=[];
  reel4:any[]=[];
  reel5:any[]=[];

  imageList:string[] = ['symbol00.png','symbol01.png','symbol02.png','symbol03.png','symbol04.png','symbol05.png','symbol06.png','symbol07.png','symbol08.png','symbol09.png','symbol10.png','symbol11.png','symbol12.png'];
  spinningList:ImageInfo[] = [{imageLink:'symbol00.png',highlight:false},{imageLink:'symbol01.png',highlight:false},{imageLink:'symbol02.png',highlight:false},{imageLink:'symbol03.png',highlight:false},{imageLink:'symbol04.png',highlight:false},{imageLink:'symbol05.png',highlight:false},{imageLink:'symbol06.png',highlight:false},{imageLink:'symbol07.png',highlight:false},{imageLink:'symbol08.png',highlight:false},{imageLink:'symbol09.png',highlight:false},{imageLink:'symbol10.png',highlight:false},{imageLink:'symbol11.png',highlight:false},{imageLink:'symbol12.png',highlight:false}];
  isMatchFound:boolean = false;

  constructor(private slot_service:SlotImageService,private router:Router,private dialog:MatDialog) {
   
   }

  ngOnInit(): void {
    this.reels = this.slot_service.getReelInitialize();
    this.reel1 = this.reels[0];
    this.reel2 = this.reels[1];
    this.reel3 = this.reels[2];
    this.reel4 = this.reels[3];
    this.reel5 = this.reels[4];
    
  }

  spinReels():void{
      this.reel1 = this.spinningList;
      this.reel2 = this.spinningList;
      this.reel3 = this.spinningList;
      this.reel4 = this.spinningList;
      this.reel5 = this.spinningList;
      this.isMatchFound = false;
      this.reels = this.randomGenerateArray();
      

      //Reel1 spinning logic
      const Reel1_Interval = setInterval(()=>{
        this.reel1.unshift(this.reel1.pop());
      },100);
      setTimeout(()=>{
        clearInterval(Reel1_Interval);
        this.reel1 = this.reels[0];
      },1500);

      //Reel2 spinning logic
      const Reel2_Interval = setInterval(()=>{
        this.reel2.unshift(this.reel2.pop());
      },100);
      setTimeout(()=>{
        clearInterval(Reel2_Interval);
        this.reel2 = this.reels[1];
      },2000);

      //Reel3 spinning Logic
      const Reel3_Interval = setInterval(()=>{
        this.reel3.unshift(this.reel3.pop());
      },100);
      setTimeout(()=>{
        clearInterval(Reel3_Interval);
        this.reel3 = this.reels[2];
      },2500);

      //Reel4 spinning Logic
      const Reel4_Interval = setInterval(()=>{
        this.reel4.unshift(this.reel4.pop());
      },100);
      setTimeout(()=>{
        clearInterval(Reel4_Interval);
        this.reel4 = this.reels[3];
      },3000);

      //Reel5 spinning logic
      const Reel5_Interval = setInterval(()=>{
        this.reel5.unshift(this.reel5.pop());
      },100);
      setTimeout(()=>{
        clearInterval(Reel5_Interval);
        this.reels = this.matchingLines(this.reels);
        this.reel1 = this.reels[0];
        this.reel2 = this.reels[1];
        this.reel3 = this.reels[2];
        this.reel4 = this.reels[3];
        this.reel5 = this.reels[4];
      },3500);

      
      
  }

  randomGenerateArray():ImageInfo[][]{
    let arr:ImageInfo[][] = [];
    for(let i=0;i<5;i++){
      let a = [];
        for(let j=0;j<3;j++){
          const randomIndex = Math.floor(Math.random()* this.imageList.length);
          let obj:ImageInfo={imageLink:this.imageList[randomIndex],highlight:false};
          a.push(obj);
        }
        arr[i] = a; 
    }
    return arr;
  }

  homepage(){
   this.router.navigate(['/']);
  }

  paylineDialog(){
    this.dialog.open(PaylinesComponent,{width:'820px',height:'470px'});
  }

  matchingLines(reelArr:ImageInfo[][]):ImageInfo[][]{

    //Line 1
    if(reelArr[0][0].imageLink===reelArr[1][0].imageLink && reelArr[0][0].imageLink===reelArr[2][0].imageLink){
      reelArr[0][0].highlight = true;
      reelArr[1][0].highlight = true;
      reelArr[2][0].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][0].imageLink===reelArr[3][0].imageLink){
        reelArr[3][0].highlight = true;
        if(reelArr[0][0].imageLink===reelArr[4][0].imageLink){
          reelArr[4][0].highlight = true;
        }
      }
    }

    //Line 2
    if(reelArr[0][1].imageLink===reelArr[1][1].imageLink && reelArr[0][1].imageLink===reelArr[2][1].imageLink){
      reelArr[0][1].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][1].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][1].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][1].imageLink===reelArr[4][1].imageLink){
          reelArr[4][1].highlight = true;
        }
      }
    }

    //Line 3
    if(reelArr[0][2].imageLink===reelArr[1][2].imageLink && reelArr[0][2].imageLink===reelArr[2][2].imageLink){
      reelArr[0][2].highlight = true;
      reelArr[1][2].highlight = true;
      reelArr[2][2].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][2].imageLink===reelArr[3][2].imageLink){
        reelArr[3][2].highlight = true;
        if(reelArr[0][2].imageLink===reelArr[4][2].imageLink){
          reelArr[4][2].highlight = true;
        }
      }
    }

    //Line 4
    if(reelArr[0][0].imageLink===reelArr[1][1].imageLink && reelArr[0][0].imageLink===reelArr[2][2].imageLink){
      reelArr[0][0].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][2].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][0].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][0].imageLink===reelArr[4][0].imageLink){
          reelArr[4][0].highlight = true;
        }
      }
    }

    //Line 5
    if(reelArr[0][2].imageLink===reelArr[1][1].imageLink && reelArr[0][2].imageLink===reelArr[2][0].imageLink){
      reelArr[0][2].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][0].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][2].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][2].imageLink===reelArr[4][2].imageLink){
          reelArr[4][2].highlight = true;
        }
      }
    }

    //Line 6
    if(reelArr[0][1].imageLink===reelArr[1][0].imageLink && reelArr[0][1].imageLink===reelArr[2][0].imageLink){
      reelArr[0][1].highlight = true;
      reelArr[1][0].highlight = true;
      reelArr[2][0].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][1].imageLink===reelArr[3][0].imageLink){
        reelArr[3][0].highlight = true;
        if(reelArr[0][1].imageLink===reelArr[4][1].imageLink){
          reelArr[4][1].highlight = true;
        }
      }
    }

    //Line 7
    if(reelArr[0][1].imageLink===reelArr[1][2].imageLink && reelArr[0][1].imageLink===reelArr[2][2].imageLink){
      reelArr[0][1].highlight = true;
      reelArr[1][2].highlight = true;
      reelArr[2][2].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][1].imageLink===reelArr[3][2].imageLink){
        reelArr[3][2].highlight = true;
        if(reelArr[0][1].imageLink===reelArr[4][1].imageLink){
          reelArr[4][1].highlight = true;
        }
      }
    }

    //Line 8
    if(reelArr[0][2].imageLink===reelArr[1][1].imageLink && reelArr[0][2].imageLink===reelArr[2][1].imageLink){
      reelArr[0][2].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][1].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][2].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][2].imageLink===reelArr[4][2].imageLink){
          reelArr[4][2].highlight = true;
        }
      }
    }

    //Line 9
    if(reelArr[0][0].imageLink===reelArr[1][1].imageLink && reelArr[0][0].imageLink===reelArr[2][1].imageLink){
      reelArr[0][0].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][1].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][0].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][0].imageLink===reelArr[4][0].imageLink){
          reelArr[4][0].highlight = true;
        }
      }
    }

    //Line 10
    if(reelArr[0][0].imageLink===reelArr[1][1].imageLink && reelArr[0][0].imageLink===reelArr[2][0].imageLink){
      reelArr[0][0].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][0].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][0].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][0].imageLink===reelArr[4][0].imageLink){
          reelArr[4][0].highlight = true;
        }
      }
    }

    //Line 11
    if(reelArr[0][2].imageLink===reelArr[1][1].imageLink && reelArr[0][2].imageLink===reelArr[2][2].imageLink){
      reelArr[0][2].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][2].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][2].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][2].imageLink===reelArr[4][2].imageLink){
          reelArr[4][2].highlight = true;
        }
      }
    }

    //Line 12
    if(reelArr[0][0].imageLink===reelArr[1][1].imageLink && reelArr[0][0].imageLink===reelArr[2][2].imageLink){
      reelArr[0][0].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][2].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][0].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][0].imageLink===reelArr[4][2].imageLink){
          reelArr[4][2].highlight = true;
        }
      }
    }

    //Line 13
    if(reelArr[0][2].imageLink===reelArr[1][1].imageLink && reelArr[0][2].imageLink===reelArr[2][0].imageLink){
      reelArr[0][2].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][0].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][2].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][2].imageLink===reelArr[4][0].imageLink){
          reelArr[4][0].highlight = true;
        }
      }
    }

    //Line 14
    if(reelArr[0][0].imageLink===reelArr[1][0].imageLink && reelArr[0][0].imageLink===reelArr[2][1].imageLink){
      reelArr[0][0].highlight = true;
      reelArr[1][0].highlight = true;
      reelArr[2][1].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][0].imageLink===reelArr[3][2].imageLink){
        reelArr[3][2].highlight = true;
        if(reelArr[0][0].imageLink===reelArr[4][2].imageLink){
          reelArr[4][2].highlight = true;
        }
      }
    }

    //Line 15
    if(reelArr[0][2].imageLink===reelArr[1][2].imageLink && reelArr[0][2].imageLink===reelArr[2][1].imageLink){
      reelArr[0][2].highlight = true;
      reelArr[1][2].highlight = true;
      reelArr[2][1].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][2].imageLink===reelArr[3][0].imageLink){
        reelArr[3][0].highlight = true;
        if(reelArr[0][2].imageLink===reelArr[4][0].imageLink){
          reelArr[4][0].highlight = true;
        }
      }
    }

    //Line 16
    if(reelArr[0][1].imageLink===reelArr[1][1].imageLink && reelArr[0][1].imageLink===reelArr[2][0].imageLink){
      reelArr[0][1].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][0].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][1].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][1].imageLink===reelArr[4][1].imageLink){
          reelArr[4][1].highlight = true;
        }
      }
    }

    //Line 17
    if(reelArr[0][1].imageLink===reelArr[1][1].imageLink && reelArr[0][1].imageLink===reelArr[2][2].imageLink){
      reelArr[0][1].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][2].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][1].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][1].imageLink===reelArr[4][1].imageLink){
          reelArr[4][1].highlight = true;
        }
      }
    }

    //Line 18
    if(reelArr[0][0].imageLink===reelArr[1][1].imageLink && reelArr[0][0].imageLink===reelArr[2][2].imageLink){
      reelArr[0][0].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][2].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][0].imageLink===reelArr[3][2].imageLink){
        reelArr[3][2].highlight = true;
        if(reelArr[0][0].imageLink===reelArr[4][2].imageLink){
          reelArr[4][2].highlight = true;
        }
      }
    }

    //Line19
    if(reelArr[0][2].imageLink===reelArr[1][1].imageLink && reelArr[0][2].imageLink===reelArr[2][0].imageLink){
      reelArr[0][2].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][0].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][2].imageLink===reelArr[3][0].imageLink){
        reelArr[3][0].highlight = true;
        if(reelArr[0][2].imageLink===reelArr[4][0].imageLink){
          reelArr[4][0].highlight = true;
        }
      }
    }

    //Line20
    if(reelArr[0][1].imageLink===reelArr[1][0].imageLink && reelArr[0][1].imageLink===reelArr[2][1].imageLink){
      reelArr[0][1].highlight = true;
      reelArr[1][0].highlight = true;
      reelArr[2][1].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][1].imageLink===reelArr[3][2].imageLink){
        reelArr[3][2].highlight = true;
        if(reelArr[0][1].imageLink===reelArr[4][1].imageLink){
          reelArr[4][1].highlight = true;
        }
      }
    }


    //Line 21
    if(reelArr[0][1].imageLink===reelArr[1][0].imageLink && reelArr[0][1].imageLink===reelArr[2][1].imageLink){
      reelArr[0][1].highlight = true;
      reelArr[1][0].highlight = true;
      reelArr[2][1].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][1].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][1].imageLink===reelArr[4][0].imageLink){
          reelArr[4][0].highlight = true;
        }
      }
    }


    //Line 22
    if(reelArr[0][2].imageLink===reelArr[1][1].imageLink && reelArr[0][2].imageLink===reelArr[2][2].imageLink){
      reelArr[0][2].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][2].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][2].imageLink===reelArr[3][2].imageLink){
        reelArr[3][2].highlight = true;
        if(reelArr[0][2].imageLink===reelArr[4][1].imageLink){
          reelArr[4][1].highlight = true;
        }
      }
    }


    //Line 23
    if(reelArr[0][0].imageLink===reelArr[1][0].imageLink && reelArr[0][0].imageLink===reelArr[2][0].imageLink){
      reelArr[0][0].highlight = true;
      reelArr[1][0].highlight = true;
      reelArr[2][0].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][0].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][0].imageLink===reelArr[4][0].imageLink){
          reelArr[4][0].highlight = true;
        }
      }
    }


    //Line 24
    if(reelArr[0][2].imageLink===reelArr[1][2].imageLink && reelArr[0][2].imageLink===reelArr[2][2].imageLink){
      reelArr[0][2].highlight = true;
      reelArr[1][2].highlight = true;
      reelArr[2][2].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][2].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][2].imageLink===reelArr[4][0].imageLink){
          reelArr[4][0].highlight = true;
        }
      }
    }


    //Line 25
    if(reelArr[0][0].imageLink===reelArr[1][0].imageLink && reelArr[0][0].imageLink===reelArr[2][0].imageLink){
      reelArr[0][0].highlight = true;
      reelArr[1][0].highlight = true;
      reelArr[2][0].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][0].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][0].imageLink===reelArr[4][2].imageLink){
          reelArr[4][2].highlight = true;
        }
      }
    }


    //Line 26
    if(reelArr[0][0].imageLink===reelArr[1][1].imageLink && reelArr[0][0].imageLink===reelArr[2][0].imageLink){
      reelArr[0][0].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][0].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][0].imageLink===reelArr[3][0].imageLink){
        reelArr[3][0].highlight = true;
        if(reelArr[0][0].imageLink===reelArr[4][1].imageLink){
          reelArr[4][1].highlight = true;
        }
      }
    }

    //Line 27
    if(reelArr[0][1].imageLink===reelArr[1][2].imageLink && reelArr[0][1].imageLink===reelArr[2][1].imageLink){
      reelArr[0][1].highlight = true;
      reelArr[1][2].highlight = true;
      reelArr[2][1].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][1].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][1].imageLink===reelArr[4][2].imageLink){
          reelArr[4][2].highlight = true;
        }
      }
    }

    //Line 28
    if(reelArr[0][1].imageLink===reelArr[1][2].imageLink && reelArr[0][1].imageLink===reelArr[2][1].imageLink){
      reelArr[0][1].highlight = true;
      reelArr[1][2].highlight = true;
      reelArr[2][1].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][1].imageLink===reelArr[3][0].imageLink){
        reelArr[3][0].highlight = true;
        if(reelArr[0][1].imageLink===reelArr[4][1].imageLink){
          reelArr[4][1].highlight = true;
        }
      }
    }


    //Line 29
    if(reelArr[0][2].imageLink===reelArr[1][1].imageLink && reelArr[0][2].imageLink===reelArr[2][2].imageLink){
      reelArr[0][2].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][2].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][2].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][2].imageLink===reelArr[4][0].imageLink){
          reelArr[4][0].highlight = true;
        }
      }
    }


    //Line 30
    if(reelArr[0][0].imageLink===reelArr[1][1].imageLink && reelArr[0][0].imageLink===reelArr[2][0].imageLink){
      reelArr[0][0].highlight = true;
      reelArr[1][1].highlight = true;
      reelArr[2][0].highlight = true;
      this.isMatchFound = true;
      if(reelArr[0][0].imageLink===reelArr[3][1].imageLink){
        reelArr[3][1].highlight = true;
        if(reelArr[0][0].imageLink===reelArr[4][2].imageLink){
          reelArr[4][2].highlight = true;
        }
      }
    }

    return reelArr;
  }

}
