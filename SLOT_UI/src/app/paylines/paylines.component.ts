import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-paylines',
  templateUrl: './paylines.component.html',
  styleUrls: ['./paylines.component.scss']
})
export class PaylinesComponent implements OnInit {

  @ViewChild('line1',{static:true}) Line1!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line2',{static:true}) Line2!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line3',{static:true}) Line3!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line4',{static:true}) Line4!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line5',{static:true}) Line5!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line6',{static:true}) Line6!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line7',{static:true}) Line7!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line8',{static:true}) Line8!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line9',{static:true}) Line9!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line10',{static:true}) Line10!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line11',{static:true}) Line11!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line12',{static:true}) Line12!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line13',{static:true}) Line13!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line14',{static:true}) Line14!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line15',{static:true}) Line15!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line16',{static:true}) Line16!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line17',{static:true}) Line17!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line18',{static:true}) Line18!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line19',{static:true}) Line19!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line20',{static:true}) Line20!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line21',{static:true}) Line21!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line22',{static:true}) Line22!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line23',{static:true}) Line23!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line24',{static:true}) Line24!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line25',{static:true}) Line25!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line26',{static:true}) Line26!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line27',{static:true}) Line27!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line28',{static:true}) Line28!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line29',{static:true}) Line29!:ElementRef<HTMLCanvasElement>;
  @ViewChild('line30',{static:true}) Line30!:ElementRef<HTMLCanvasElement>;


  constructor() { }

  ngOnInit(): void {
    const PayLine1 = this.Line1.nativeElement.getContext('2d');
    this.drawLine1(PayLine1!);
    const PayLine2 = this.Line2.nativeElement.getContext('2d');
    this.drawLine2(PayLine2!);
    const PayLine3 = this.Line3.nativeElement.getContext('2d');
    this.drawLine3(PayLine3!);
    const PayLine4 = this.Line4.nativeElement.getContext('2d');
    this.drawLine4(PayLine4!);
    const PayLine5 = this.Line5.nativeElement.getContext('2d');
    this.drawLine5(PayLine5!);
    const PayLine6 = this.Line6.nativeElement.getContext('2d');
    this.drawLine6(PayLine6!);
    const PayLine7 = this.Line7.nativeElement.getContext('2d');
    this.drawLine7(PayLine7!);
    const PayLine8 = this.Line8.nativeElement.getContext('2d');
    this.drawLine8(PayLine8!);
    const PayLine9 = this.Line9.nativeElement.getContext('2d');
    this.drawLine9(PayLine9!);
    const PayLine10 = this.Line10.nativeElement.getContext('2d');
    this.drawLine10(PayLine10!);
    const PayLine11 = this.Line11.nativeElement.getContext('2d');
    this.drawLine11(PayLine11!);
    const PayLine12 = this.Line12.nativeElement.getContext('2d');
    this.drawLine12(PayLine12!);
    const PayLine13 = this.Line13.nativeElement.getContext('2d');
    this.drawLine13(PayLine13!);
    const PayLine14 = this.Line14.nativeElement.getContext('2d');
    this.drawLine14(PayLine14!);
    const PayLine15 = this.Line15.nativeElement.getContext('2d');
    this.drawLine15(PayLine15!);
    const PayLine16 = this.Line16.nativeElement.getContext('2d');
    this.drawLine16(PayLine16!);
    const PayLine17 = this.Line17.nativeElement.getContext('2d');
    this.drawLine17(PayLine17!);
    const PayLine18 = this.Line18.nativeElement.getContext('2d');
    this.drawLine18(PayLine18!);
    const PayLine19 = this.Line19.nativeElement.getContext('2d');
    this.drawLine19(PayLine19!);
    const PayLine20 = this.Line20.nativeElement.getContext('2d');
    this.drawLine20(PayLine20!);
    const PayLine21 = this.Line21.nativeElement.getContext('2d');
    this.drawLine21(PayLine21!);
    const PayLine22 = this.Line22.nativeElement.getContext('2d');
    this.drawLine22(PayLine22!);
    const PayLine23 = this.Line23.nativeElement.getContext('2d');
    this.drawLine23(PayLine23!);
    const PayLine24 = this.Line24.nativeElement.getContext('2d');
    this.drawLine24(PayLine24!);
    const PayLine25 = this.Line25.nativeElement.getContext('2d');
    this.drawLine25(PayLine25!);
    const PayLine26 = this.Line26.nativeElement.getContext('2d');
    this.drawLine26(PayLine26!);
    const PayLine27 = this.Line27.nativeElement.getContext('2d');
    this.drawLine27(PayLine27!);
    const PayLine28 = this.Line28.nativeElement.getContext('2d');
    this.drawLine28(PayLine28!);
    const PayLine29 = this.Line29.nativeElement.getContext('2d');
    this.drawLine29(PayLine29!);
    const PayLine30 = this.Line30.nativeElement.getContext('2d');
    this.drawLine30(PayLine30!);
    

  }

  drawLine1(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===0 && col===0)||(row===0 && col===1)||(row===0 && col===2)||(row===0 && col===3)||(row===0 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine2(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===1 && col===0)||(row===1 && col===1)||(row===1 && col===2)||(row===1 && col===3)||(row===1 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }


  drawLine3(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===2 && col===0)||(row===2 && col===1)||(row===2 && col===2)||(row===2 && col===3)||(row===2 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine4(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===0 && col===0)||(row===1 && col===1)||(row===2 && col===2)||(row===1 && col===3)||(row===0 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }


  drawLine5(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===2 && col===0)||(row===1 && col===1)||(row===0 && col===2)||(row===1 && col===3)||(row===2 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }


  drawLine6(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===1 && col===0)||(row===0 && col===1)||(row===0 && col===2)||(row===0 && col===3)||(row===1 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }


  drawLine7(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===1 && col===0)||(row===2 && col===1)||(row===2 && col===2)||(row===2 && col===3)||(row===1 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }


  drawLine8(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===2 && col===0)||(row===1 && col===1)||(row===1 && col===2)||(row===1 && col===3)||(row===2 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }


  drawLine9(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===0 && col===0)||(row===1 && col===1)||(row===1 && col===2)||(row===1 && col===3)||(row===0 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine10(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===0 && col===0)||(row===1 && col===1)||(row===0 && col===2)||(row===1 && col===3)||(row===0 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine11(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===2 && col===0)||(row===1 && col===1)||(row===2 && col===2)||(row===1 && col===3)||(row===2 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }


  drawLine12(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===0 && col===0)||(row===1 && col===1)||(row===2 && col===2)||(row===1 && col===3)||(row===2 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }


  drawLine13(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===2 && col===0)||(row===1 && col===1)||(row===0 && col===2)||(row===1 && col===3)||(row===0 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine14(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===0 && col===0)||(row===0 && col===1)||(row===1 && col===2)||(row===2 && col===3)||(row===2 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }


  drawLine15(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===2 && col===0)||(row===2 && col===1)||(row===1 && col===2)||(row===0 && col===3)||(row===0 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }


  drawLine16(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===1 && col===0)||(row===1 && col===1)||(row===0 && col===2)||(row===1 && col===3)||(row===1 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine17(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===1 && col===0)||(row===1 && col===1)||(row===2 && col===2)||(row===1 && col===3)||(row===1 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine18(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===0 && col===0)||(row===1 && col===1)||(row===2 && col===2)||(row===2 && col===3)||(row===2 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }


  drawLine19(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===2 && col===0)||(row===1 && col===1)||(row===0 && col===2)||(row===0 && col===3)||(row===0 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine20(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===1 && col===0)||(row===0 && col===1)||(row===1 && col===2)||(row===2 && col===3)||(row===1 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine21(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===1 && col===0)||(row===0 && col===1)||(row===1 && col===2)||(row===1 && col===3)||(row===0 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine22(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===2 && col===0)||(row===1 && col===1)||(row===2 && col===2)||(row===2 && col===3)||(row===1 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine23(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===0 && col===0)||(row===0 && col===1)||(row===0 && col===2)||(row===1 && col===3)||(row===0 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine24(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===2 && col===0)||(row===2 && col===1)||(row===2 && col===2)||(row===1 && col===3)||(row===0 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine25(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===0 && col===0)||(row===0 && col===1)||(row===0 && col===2)||(row===1 && col===3)||(row===2 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine26(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===0 && col===0)||(row===1 && col===1)||(row===0 && col===2)||(row===0 && col===3)||(row===1 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }
  
  drawLine27(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===1 && col===0)||(row===2 && col===1)||(row===1 && col===2)||(row===1 && col===3)||(row===2 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine28(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===1 && col===0)||(row===2 && col===1)||(row===1 && col===2)||(row===0 && col===3)||(row===1 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine29(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===2 && col===0)||(row===1 && col===1)||(row===2 && col===2)||(row===1 && col===3)||(row===0 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }

  drawLine30(ctElem:CanvasRenderingContext2D):void{

    const cellwidth = this.Line1.nativeElement.width/5;
    const cellheight = this.Line1.nativeElement.height/3;
    
    for(let row =0;row<3;row++){
      for(let col=0;col<5;col++){
        const x = col * cellwidth;
        const y = row * cellheight;

        ctElem.strokeRect(x,y,cellwidth,cellheight);
        if((row===0 && col===0)||(row===1 && col===1)||(row===0 && col===2)||(row===1 && col===3)||(row===2 && col===4)){
          ctElem.fillStyle = 'blue';
          ctElem.fillRect(x,y,cellwidth,cellheight);
        }
      }
    }
  }


}
