import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaylinesComponent } from './paylines.component';

describe('PaylinesComponent', () => {
  let component: PaylinesComponent;
  let fixture: ComponentFixture<PaylinesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaylinesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PaylinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
