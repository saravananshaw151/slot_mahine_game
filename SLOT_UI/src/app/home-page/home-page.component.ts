import { trigger,transition,style,animate,state } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';



@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']

})
export class HomePageComponent implements OnInit {
  
  constructor(private router:Router) { }

  ngOnInit(): void {
  }


  slotPageLink(){
    this.router.navigate(['/slot_game']);
  }
  
 

}
