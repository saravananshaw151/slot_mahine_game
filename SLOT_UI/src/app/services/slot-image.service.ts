import { Injectable } from '@angular/core';

interface ImageInfo{
  imageLink:string,
  highlight:boolean
}

@Injectable({
  providedIn: 'root'
})
export class SlotImageService {

  reelObjects:ImageInfo[][] = [[{imageLink:'symbol00.png',highlight:false},{imageLink:'symbol01.png',highlight:false},{imageLink:'symbol02.png',highlight:false}],
                            [{imageLink:'symbol03.png',highlight:false},{imageLink:'symbol04.png',highlight:false},{imageLink:'symbol05.png',highlight:false}],
                            [{imageLink:'symbol06.png',highlight:false},{imageLink:'symbol07.png',highlight:false},{imageLink:'symbol08.png',highlight:false}],
                            [{imageLink:'symbol09.png',highlight:false},{imageLink:'symbol10.png',highlight:false},{imageLink:'symbol11.png',highlight:false}],
                            [{imageLink:'symbol12.png',highlight:false},{imageLink:'symbol05.png',highlight:false},{imageLink:'symbol02.png',highlight:false}]];
  constructor() { }

  getReelInitialize():ImageInfo[][]{
    return this.reelObjects;
  }
}
