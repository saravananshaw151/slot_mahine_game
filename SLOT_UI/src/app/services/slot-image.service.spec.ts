import { TestBed } from '@angular/core/testing';

import { SlotImageService } from './slot-image.service';

describe('SlotImageService', () => {
  let service: SlotImageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SlotImageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
